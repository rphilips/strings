package rstrings

import (
	"bytes"
	"unicode"
	"unicode/utf8"

	unidecode "github.com/mozillazg/go-unidecode"
	"golang.org/x/text/unicode/norm"
)

var chars = map[rune]string{
	8364: "EUR",
	8226: "-",
}

// ASCII tries to represent a Unicode string in ASCII
func ASCII(s string) string {
	if s == "" {
		return ""
	}
	s = norm.NFC.String(s)
	return unidecode.Unidecode(s)
}

// Latin1 tries to represent a Unicode string in Latin1
func Latin1(s string) string {
	if s == "" {
		return ""
	}
	s = norm.NFC.String(s)
	max := unicode.MaxLatin1
	var buffer bytes.Buffer
	for s != "" {
		ch, size := utf8.DecodeRuneInString(s)
		s = s[size:]
		switch {
		case ch <= max:
			buffer.WriteRune(ch)
		case chars[ch] != "":
			buffer.WriteString(chars[ch])
		default:
			buffer.WriteString(unidecode.Unidecode(string(ch)))
		}
	}
	return buffer.String()
}
