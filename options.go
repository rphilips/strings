package rstrings

import "fmt"

type Config struct {
	prefix     string
	suffix     string
	escape     rune
	notescaped bool
	id         string
	keepescape bool
	maxLen     int
}

type fc = func(*Config)
type ffc = func(bool) func(*Config)

type Option any

func Options(options ...Option) []Option {

	if len(options) == 0 {
		return nil
	}
	return options
}

func NewCfg(opts []Option) Config {
	c := new(Config)
	c.escape = '\\'
	c.prefix = "{"
	c.suffix = "}"
	for _, opt := range opts {
		switch op := opt.(type) {
		case fc:
			op(c)
		case ffc:
			op(true)(c)
		default:
			panic(fmt.Errorf("option %v is of bad type", op))
		}
	}
	return *c
}

func WithMaxLen(maxlen int) fc {
	return func(c *Config) {
		c.maxLen = maxlen
	}
}

func WithPrefix(prefix string) fc {
	return func(c *Config) {
		c.prefix = prefix
	}
}

func WithSuffix(suffix string) fc {
	return func(c *Config) {
		c.suffix = suffix
	}
}

func WithEscapeRune(escape rune) fc {
	return func(c *Config) {
		c.escape = escape
	}
}

func WithId(id string) fc {
	return func(c *Config) {
		c.id = id
	}
}

func WithKeepEscape(tf bool) fc {
	return func(c *Config) {
		c.keepescape = tf
	}
}

func WithNotEscaped(tf bool) fc {
	return func(c *Config) {
		c.notescaped = tf
	}
}
