module rstrings

go 1.23.4

require (
	github.com/huandu/xstrings v1.4.0
	github.com/mozillazg/go-unidecode v0.2.0
	golang.org/x/text v0.14.0
)
